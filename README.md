# Foundry Virtual Tabletop - World Anvil Integration - Classic Journal Entry display

This module allows to have a similar display between Foundry Journal Entry and [World Anvil](https://worldanvil.com) classic theme .

It comes on top of the core module [World Anvil Integration](https://gitlab.com/foundrynet/world-anvil/-/raw/master/module.json)

-----

## Installation

This module can be installed by using the following module manifest url: (https://gitlab.com/adrien.schiehle/world-anvil-css-classic/-/raw/master/module.json).
